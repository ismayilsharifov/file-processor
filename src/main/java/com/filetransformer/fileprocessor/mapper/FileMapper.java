package com.filetransformer.fileprocessor.mapper;

import com.filetransformer.fileprocessor.dao.entity.TransformedFile;
import com.filetransformer.fileprocessor.model.response.OriginalFileResponseDto;
import com.filetransformer.fileprocessor.dao.entity.OriginalFile;
import com.filetransformer.fileprocessor.model.response.TransformedFileResponseDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface FileMapper {

    OriginalFileResponseDto mapOriginalFileResponse(OriginalFile originalFile);

    List<OriginalFileResponseDto> mapOriginalFileListResponse(List<OriginalFile> originalFiles);

    TransformedFileResponseDto mapTransformedFileResponse(TransformedFile transformedFile);

    List<TransformedFileResponseDto> mapTransformedFileListResponse(List<TransformedFile> transformedFiles);
}
