package com.filetransformer.fileprocessor.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "transformed_file")
@NoArgsConstructor
public class TransformedFile {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    @Column(name = "original_file_id")
    private String originalFileId;
    private String name;
    private String type;
    @Lob
    private byte[] data;

    @Column(name = "receipt_date")
    @CreationTimestamp
    private LocalDateTime receiptDate;

    public TransformedFile(String name, String type, byte[] data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }
}
