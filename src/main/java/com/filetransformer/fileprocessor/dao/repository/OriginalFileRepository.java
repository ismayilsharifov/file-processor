package com.filetransformer.fileprocessor.dao.repository;

import com.filetransformer.fileprocessor.dao.entity.OriginalFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OriginalFileRepository extends JpaRepository<OriginalFile, String> {
}
