package com.filetransformer.fileprocessor.dao.repository;

import com.filetransformer.fileprocessor.dao.entity.TransformedFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransformedFileRepository extends JpaRepository<TransformedFile, String> {
}
