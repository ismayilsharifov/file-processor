package com.filetransformer.fileprocessor.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OriginalResponseDto {
    private String id;
    private String name;
    private String uri;
    private String type;
    private LocalDateTime receiptDate;
    private long size;
}
