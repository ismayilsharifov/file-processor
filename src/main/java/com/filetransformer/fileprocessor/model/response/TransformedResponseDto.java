package com.filetransformer.fileprocessor.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class TransformedResponseDto {
    private String id;
    private String originalFileId;
    private String name;
    private String uri;
    private String originalFileUri;
    private String type;
    private LocalDateTime receiptDate;
    private long size;
}
