package com.filetransformer.fileprocessor.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OriginalFileResponseDto {
    private String id;
    private String name;
    private String uri;
    private String type;
    private LocalDateTime receiptDate;
    private byte[] data;
    private long size;

}
