package com.filetransformer.fileprocessor.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;


@Data
@Builder
public class TransformedFileResponseDto {

    private String id;
    private String originalFileId;
    private String name;
    private String uri;
    private String originalFileUrl;
    private String type;
    private LocalDateTime receiptDate;
    private byte[] data;
    private long size;
}
