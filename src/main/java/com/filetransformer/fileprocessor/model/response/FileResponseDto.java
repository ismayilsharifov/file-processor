package com.filetransformer.fileprocessor.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class FileResponseDto {
    private String originalFileId;
    private String transformedFileId;
    private String originalFileName;
    private String originalFileUri;
    private String transformedFileUri;
    private String type;
    private LocalDateTime receiptDate;
    private long originalFileSize;
    private long transformedFileSize;
}
