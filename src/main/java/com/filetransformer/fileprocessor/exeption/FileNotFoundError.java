package com.filetransformer.fileprocessor.exeption;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
public class FileNotFoundError {
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;

    private FileNotFoundError() {
        timestamp = LocalDateTime.now();
    }

    FileNotFoundError(HttpStatus status) {
        this();
        this.status = status;
    }

    FileNotFoundError(HttpStatus status, String message) {
        this();
        this.status = status;
        this.message = message;
    }
}
