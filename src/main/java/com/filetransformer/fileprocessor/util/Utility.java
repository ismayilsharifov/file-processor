package com.filetransformer.fileprocessor.util;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class Utility {
    public static String getOrgDownloadUri(String fileId){
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/file/")
                .path("original_files/")
                .path(fileId)
                .toUriString();
    }

    public static String getTrDownloadUri(String fileId){
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/file/")
                .path("transformed_files/")
                .path(fileId)
                .toUriString();
    }
}
