package com.filetransformer.fileprocessor.service;

import com.filetransformer.fileprocessor.model.response.FileResponseDto;
import com.filetransformer.fileprocessor.model.response.OriginalFileResponseDto;
import com.filetransformer.fileprocessor.model.response.OriginalResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedFileResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public interface FileService {

    FileResponseDto storeFiles(MultipartFile xmlFile, MultipartFile xslFile) throws IOException;

    OriginalFileResponseDto getOriginalFileById(String id);

    TransformedFileResponseDto getTransformedFileById(String id);

    List<OriginalResponseDto> getAllOriginalFiles();

    List<TransformedResponseDto> getAllTransformedFiles();

    void deleteOriginalFile(String id);

    void deleteTransformedFile(String id);


}
