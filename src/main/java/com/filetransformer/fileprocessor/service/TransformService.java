package com.filetransformer.fileprocessor.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static javax.xml.transform.TransformerFactory.newInstance;

@Service
@RequiredArgsConstructor
public class TransformService {
    public byte[] transform(MultipartFile inputXmlFile, MultipartFile xslFile) {
        StreamSource xslCode;
        StreamSource inputXML;
        try {
            xslCode = new StreamSource(xslFile.getInputStream());
            inputXML = new StreamSource(inputXmlFile.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        TransformerFactory tf = newInstance();
        Transformer transformer;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        StreamResult result = new StreamResult(bos);
        try {
            transformer = tf.newTransformer(xslCode);
            transformer.transform(inputXML, result);
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
        return bos.toByteArray();
    }
}

