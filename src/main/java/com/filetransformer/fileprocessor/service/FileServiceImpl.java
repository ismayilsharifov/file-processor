package com.filetransformer.fileprocessor.service;

import com.filetransformer.fileprocessor.dao.entity.OriginalFile;
import com.filetransformer.fileprocessor.dao.entity.TransformedFile;
import com.filetransformer.fileprocessor.model.response.FileResponseDto;
import com.filetransformer.fileprocessor.model.response.OriginalFileResponseDto;
import com.filetransformer.fileprocessor.mapper.FileMapper;
import com.filetransformer.fileprocessor.model.response.OriginalResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedFileResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedResponseDto;
import com.filetransformer.fileprocessor.dao.repository.OriginalFileRepository;
import com.filetransformer.fileprocessor.dao.repository.TransformedFileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private final OriginalFileRepository originalFileRepository;

    private final TransformedFileRepository transformedFileRepository;
    private final ConvertingService convertingService;
    private final TransformService transformService;

    private final FileMapper fileMapper;

    @Override
    public FileResponseDto storeFiles(MultipartFile inputXmlFile, MultipartFile xslFile) throws IOException {
        var outputXmlByteArray = transformService.transform(inputXmlFile, xslFile);
        var fileName = StringUtils.cleanPath(Objects.requireNonNull(inputXmlFile.getOriginalFilename()));
        OriginalFile originalFile;
        originalFile = new OriginalFile(fileName, inputXmlFile.getContentType(), inputXmlFile.getBytes());
        originalFile = originalFileRepository.save(originalFile);
        var transformedFile = new TransformedFile("tra_" + originalFile.getReceiptDate().getNano()
                + fileName, inputXmlFile.getContentType(), outputXmlByteArray);
        transformedFile.setOriginalFileId(originalFile.getId());
        transformedFile = transformedFileRepository.save(transformedFile);
        return convertingService.makeFileResponse(originalFile, transformedFile);
    }

    @Override
    public List<OriginalResponseDto> getAllOriginalFiles() {
        return originalFileRepository.findAll().stream().map(
                        convertingService::makeOriginalResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<TransformedResponseDto> getAllTransformedFiles() {
        return transformedFileRepository.findAll().stream().map(
                        convertingService::makeTransformedResponse)
                .collect(Collectors.toList());
    }

    @Override
    public OriginalFileResponseDto getOriginalFileById(String id) {
        var file = originalFileRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("File not found with parameter {" + id + "}"));
        return fileMapper.mapOriginalFileResponse(file);
    }

    @Override
    public TransformedFileResponseDto getTransformedFileById(String id) {
        var file = transformedFileRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("File not found with id : " + id));
        return fileMapper.mapTransformedFileResponse(file);
    }

    @Override
    public void deleteOriginalFile(String id) {
        originalFileRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("File not found with id : " + id));
        originalFileRepository.deleteById(id);
    }

    @Override
    public void deleteTransformedFile(String id) {
        transformedFileRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("File not found with id : " + id));
        transformedFileRepository.deleteById(id);
    }

}
