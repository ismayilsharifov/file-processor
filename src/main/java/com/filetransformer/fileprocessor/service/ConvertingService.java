package com.filetransformer.fileprocessor.service;

import com.filetransformer.fileprocessor.dao.entity.OriginalFile;
import com.filetransformer.fileprocessor.dao.entity.TransformedFile;
import com.filetransformer.fileprocessor.model.response.FileResponseDto;
import com.filetransformer.fileprocessor.model.response.OriginalResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedResponseDto;
import com.filetransformer.fileprocessor.util.Utility;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConvertingService {
    public OriginalResponseDto makeOriginalResponse(OriginalFile file) {
        String fileDownloadUri = Utility.getOrgDownloadUri(file.getId());
        return OriginalResponseDto.builder()
                .id(file.getId())
                .name(file.getName())
                .uri(fileDownloadUri)
                .type(file.getType())
                .receiptDate(file.getReceiptDate())
                .size(file.getData().length).build();
    }

    public TransformedResponseDto makeTransformedResponse(TransformedFile file) {
        String fileDownloadUri = Utility.getTrDownloadUri(file.getId());
        String orgDownloadUri = Utility.getOrgDownloadUri(file.getOriginalFileId());
        return TransformedResponseDto.builder()
                .id(file.getId())
                .originalFileId(file.getOriginalFileId())
                .name(file.getName())
                .uri(fileDownloadUri)
                .originalFileUri(orgDownloadUri)
                .type(file.getType())
                .receiptDate(file.getReceiptDate())
                .size(file.getData().length).build();
    }

    public FileResponseDto makeFileResponse(OriginalFile originalFile, TransformedFile transformedFile) {
        String fileDownloadUri = Utility.getOrgDownloadUri(originalFile.getId());
        String transformedDownloadUri = Utility.getTrDownloadUri(transformedFile.getId());
        return FileResponseDto.builder()
                .originalFileId(originalFile.getId())
                .transformedFileId(transformedFile.getId())
                .originalFileName(originalFile.getName())
                .originalFileUri(fileDownloadUri)
                .transformedFileUri(transformedDownloadUri)
                .type(originalFile.getType())
                .receiptDate(originalFile.getReceiptDate())
                .originalFileSize(originalFile.getData().length)
                .transformedFileSize(transformedFile.getData().length).build();
    }
}
