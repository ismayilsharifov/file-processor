package com.filetransformer.fileprocessor.controller;

import com.filetransformer.fileprocessor.model.response.OriginalResponseDto;
import com.filetransformer.fileprocessor.model.response.ResponseMessage;
import com.filetransformer.fileprocessor.model.response.TransformedResponseDto;
import com.filetransformer.fileprocessor.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileProcessorController {

    private final FileService fileService;

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("xmlFile") MultipartFile xmlFile,
                                                      @RequestParam("xslFile") MultipartFile xslFile) {
        String message;
        try {
            var fileResponseDto = fileService.storeFiles(xmlFile, xslFile);
            message = "Uploaded the file " + xmlFile.getOriginalFilename() + " (size " + fileResponseDto.getOriginalFileSize() +
                    ") successfully with id : " + fileResponseDto.getOriginalFileId() + " and transformed to file (size "
                    + fileResponseDto.getTransformedFileSize() + ") with id : " + fileResponseDto.getTransformedFileId();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the files";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @GetMapping("/original_files")
    public ResponseEntity<List<OriginalResponseDto>> getListOriginalFiles() {
        return ResponseEntity.status(HttpStatus.OK).body(fileService.getAllOriginalFiles());
    }

    @GetMapping("/transformed_files")
    public ResponseEntity<List<TransformedResponseDto>> getListTransformedFiles() {
        return ResponseEntity.status(HttpStatus.OK).body(fileService.getAllTransformedFiles());
    }

    @GetMapping("/original_files/{id}")
    public ResponseEntity<byte[]> getOriginalFile(@PathVariable String id) {
        var responseDto = fileService.getOriginalFileById(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + responseDto.getName() + "\"")
                .body(responseDto.getData());
    }

    @GetMapping("/transformed_files/{id}")
    public ResponseEntity<byte[]> getTransformedFile(@PathVariable String id) {
        var responseDto = fileService.getTransformedFileById(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + responseDto.getName() + "\"")
                .body(responseDto.getData());
    }

    @DeleteMapping("/original_files/{id}")
    public ResponseEntity<String> deleteOriginalFile(@PathVariable String id) {
        fileService.deleteOriginalFile(id);
        return new ResponseEntity<>(id, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/transformed_files/{id}")
    public ResponseEntity<String> deleteTransformedFile(@PathVariable String id) {
        fileService.deleteTransformedFile(id);
        return new ResponseEntity<>(id, HttpStatus.ACCEPTED);
    }
}
