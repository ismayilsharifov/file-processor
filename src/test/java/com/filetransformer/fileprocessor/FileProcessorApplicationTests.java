package com.filetransformer.fileprocessor;

import com.filetransformer.fileprocessor.controller.FileProcessorController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FileProcessorApplicationTests {

    @Autowired
    private FileProcessorController controller;

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }

}
