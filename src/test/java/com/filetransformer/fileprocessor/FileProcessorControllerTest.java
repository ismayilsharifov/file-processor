package com.filetransformer.fileprocessor;

import com.filetransformer.fileprocessor.model.response.OriginalFileResponseDto;
import com.filetransformer.fileprocessor.model.response.OriginalResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedFileResponseDto;
import com.filetransformer.fileprocessor.model.response.TransformedResponseDto;
import com.filetransformer.fileprocessor.service.FileService;
import com.filetransformer.fileprocessor.controller.FileProcessorController;
import org.aspectj.lang.annotation.Before;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class FileProcessorControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    FileProcessorController fileProcessorController;
    @Mock
    private FileService fileService;

    @Before("")
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(fileProcessorController)
                .build();
    }

    @Test
    void testFileUploaded_verifyStatus() throws Exception {
        MockMultipartFile xmlFile = new MockMultipartFile(
                "uploaded-xmlFile",
                "file-mock.xml",
                MediaType.TEXT_PLAIN_VALUE,
                "This is the file content".getBytes());
        MockMultipartFile xslFile = new MockMultipartFile(
                "uploaded-xslFile",
                "file-mock.xsl",
                MediaType.TEXT_PLAIN_VALUE,
                "This is the file content".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/file/upload")
                        .file(xmlFile)
                        .file(xslFile))
                .andExpect(status().is(200));
    }

    @Test
    void getListOriginalFiles() throws Exception {
        List<OriginalResponseDto> files = Collections.singletonList(
                OriginalResponseDto.builder()
                        .id("1")
                        .name("uploaded-file")
                        .uri("/file/original_files")
                        .type("application/xml")
                        .receiptDate(LocalDateTime.now())
                        .size("file data".getBytes().length).build());

        Mockito.when(fileService.getAllOriginalFiles()).thenReturn(files);
        mockMvc.perform(MockMvcRequestBuilders.get("/file/original_files"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void getListTransformedFiles() throws Exception {
        List<TransformedResponseDto> files = Collections.singletonList(
                TransformedResponseDto.builder()
                        .id("1")
                        .originalFileId("1")
                        .name("uploaded-file")
                        .uri("/file/transformed_files")
                        .originalFileUri("/file/original_files")
                        .type("application/xml")
                        .receiptDate(LocalDateTime.now())
                        .size("file data".getBytes().length).build());

        Mockito.when(fileService.getAllTransformedFiles()).thenReturn(files);
        mockMvc.perform(MockMvcRequestBuilders.get("/file/transformed_files"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void getOriginalFile_verifyById() throws Exception {
        OriginalFileResponseDto originalFileResponseDto =
                OriginalFileResponseDto.builder()
                        .id("1")
                        .name("uploaded-file")
                        .uri("/file/original_files")
                        .type("application/xml")
                        .receiptDate(LocalDateTime.now())
                        .data("file data".getBytes())
                        .size("file data".getBytes().length).build();
        Mockito.when(fileService.getOriginalFileById("1")).thenReturn(originalFileResponseDto);
        mockMvc.perform(MockMvcRequestBuilders.get("/original_files/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("uploaded-file")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.uri", Matchers.is("/file/original_files/1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type", Matchers.is("application/xml")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", Matchers.is("file data".getBytes())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size", Matchers.is("file data".getBytes().length)));
        Mockito.verify(fileService).getOriginalFileById("1");
    }

    @Test
    void getTransformedFile_verifyById() throws Exception {
        TransformedFileResponseDto transformedFileResponseDto =
                TransformedFileResponseDto.builder()
                        .id("1")
                        .originalFileId("1")
                        .name("uploaded-file")
                        .uri("/file/transformed_files/1")
                        .originalFileUrl("/file/original_files/1")
                        .type("application/xml")
                        .receiptDate(LocalDateTime.now())
                        .data("file data".getBytes())
                        .size("file data".getBytes().length).build();
        Mockito.when(fileService.getTransformedFileById("1")).thenReturn(transformedFileResponseDto);
        mockMvc.perform(MockMvcRequestBuilders.get("/original_files/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.originalFileId", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("uploaded-file")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.uri", Matchers.is("/file/transformed_files/1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.originalFileUrl", Matchers.is("/file/original_files/1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type", Matchers.is("application/xml")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", Matchers.is("file data".getBytes())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size", Matchers.is("file data".getBytes().length)));
        Mockito.verify(fileService).getTransformedFileById("1");
    }

    @Test
    void deleteOriginalFile() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/original_files/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isAccepted());
    }

    @Test
    void deleteTransformedFile() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/transformed_files/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isAccepted());
    }
}