A "file-processor" project built with Spring Boot

**Description**
_API:_****

- Import files (xml and xsl) for transform.

- Fetch all original files.

- Fetch one original file.

- Fetch all transformed files.

- Fetch one transformed file.

- Delete an original file.

- Delete an transformed file

**Features:**


- A client can import a xml and xsl file for xslt transform in the service. 

- A client can view all details (receipt date, name, type, size, content, download uri) for original and transformed files.

- A client can download original and transformed files.

- A client can delete original and transformed files.


**Backlog**~~~~


- Unit tests.

- In memory database (H2).

- Implement a permanent storage service.

- Xslt transforming for xml file.

- Custom exception handler.

- Mapping dto and domain classes with Mapstruct.

- Object relational mapping JPA.

- Extend (and refactor) the REST api.
